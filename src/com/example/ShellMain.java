package com.example;

import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class ShellMain extends JFrame {

	private KnowledgeBase knowledgeBase = new KnowledgeBase();

	private final Set<String> confirmedFacts = new HashSet<>();
	private final Set<String> deniedFacts = new HashSet<>();

	private final Set<Principle> log = new LinkedHashSet<>();

	private SearchAlgorithm currentAlgorithm = SearchAlgorithm.WIDTH;
	private SearchDirection currentDirection = SearchDirection.STRAIGHT;


	public ShellMain() throws HeadlessException, SAXException, ParserConfigurationException, IOException {
		super();
		this.setSize(new Dimension(640, 480));
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setTitle("Shell for expert system");
		prepareView();
	}

	private void prepareView() throws ParserConfigurationException, SAXException, IOException {
		final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
		parser.parse(new File("data.txt"), new PrincipleHandler(knowledgeBase));
		addMenu();
		this.repaint();
	}

	private void addMenu() {
		final JMenuBar bar = new JMenuBar();
		final JMenu menu = new JMenu("Shell");
		bar.add(menu);
		final JMenuItem start = new JMenuItem("Start");
		menu.add(start);
		start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				startSearch();
			}
		});

		menu.addSeparator();
		final ButtonGroup algorithmsGroup = new ButtonGroup();
		final JMenuItem breadthSearch = new JRadioButtonMenuItem("Breadth first search");
		breadthSearch.setSelected(true);
		menu.add(breadthSearch);
		breadthSearch.addActionListener(new AlgorithmActionListener(SearchAlgorithm.WIDTH));
		algorithmsGroup.add(breadthSearch);
		final JMenuItem depthSearch = new JRadioButtonMenuItem("Depth first search");
		depthSearch.addActionListener(new AlgorithmActionListener(SearchAlgorithm.DEPTH));
		algorithmsGroup.add(depthSearch);
		menu.add(depthSearch);

		menu.addSeparator();
		final ButtonGroup directionGroup = new ButtonGroup();
		final JMenuItem straight = new JRadioButtonMenuItem("Straight search");
		straight.setSelected(true);
		straight.addActionListener(new DirectionActionListener(SearchDirection.STRAIGHT));
		directionGroup.add(straight);
		menu.add(straight);
		final JMenuItem reverse = new JRadioButtonMenuItem("Reverse search");
		reverse.addActionListener(new DirectionActionListener(SearchDirection.REVERSE));
		directionGroup.add(reverse);
		menu.add(reverse);

		this.setJMenuBar(bar);
	}

	private void startSearch() {
		if (currentDirection == SearchDirection.STRAIGHT) {
			straightSearch();
		} else {
			reverseSearch();
		}
	}

	private void reverseSearch() {
        List<KnowledgeBase> toCheck = new ArrayList<>();
        for (final String outConclusion : getEndConclusions()) {
            toCheck.add(knowledgeBase.getSubKnowledgeBaseFromConclusion(outConclusion));
        }
        for (final KnowledgeBase base : toCheck) {
            this.knowledgeBase = base;
            makeSearch();
        }
        JOptionPane.showMessageDialog(this, "Suitable result isn't found");
        System.exit(0);
	}

	private void straightSearch() {
        makeSearch();
        JOptionPane.showMessageDialog(this, "Suitable result isn't found");
		System.exit(0);
	}

    private void makeSearch() {
        for (final String conclusion : getFirstConclusions()) {
            if (checkConclusion(conclusion)) {
                if (currentAlgorithm == SearchAlgorithm.WIDTH) {
                    processConfirmedConclusionBreathStraight(conclusion);
                } else {
                    processConfirmedConclusionDepthStraight(conclusion);
                }
            }
        }
    }

    private List<String> getEndConclusions() {
		final List<String> res = new ArrayList<>();
		for (final Principle principle : knowledgeBase.getAllHypotheses()) {
			res.add(principle.getConclusion());
		}
		return res;
	}

	private List<String> getFirstConclusions() {
		final List<String> res = new ArrayList<>();
		for (final Principle principle : knowledgeBase.getAllInitialPrinciples()) {
			res.add(principle.getConclusion());
		}
		return res;
	}

	private void processConfirmedConclusionDepthStraight(final String conclusion) {
   		List<Principle> linkedPrinciples = knowledgeBase.getPrinciplesWithPremise(conclusion);
		if (linkedPrinciples.isEmpty()) {
			final ResultDialog dialog = new ResultDialog(this, LogItem.createLog(log, knowledgeBase), conclusion);
			dialog.setVisible(true);
			System.exit(0);
		}
		for (final Principle principle : linkedPrinciples) {
			if (checkConclusion(principle.getConclusion())) {
				processConfirmedConclusionDepthStraight(principle.getConclusion());
			}
		}
	}

	private void processConfirmedConclusionBreathStraight(final String conclusion) {
		List<Principle> linkedPrinciples = knowledgeBase.getPrinciplesWithPremise(conclusion);
		List<Principle> confirmedPrinciples = new ArrayList<>(linkedPrinciples.size());
		if (linkedPrinciples.isEmpty()) {
			final ResultDialog dialog = new ResultDialog(this, LogItem.createLog(log, knowledgeBase), conclusion);
			dialog.setVisible(true);
			System.exit(0);
		}
		for (final Principle principle : linkedPrinciples) {
			if (checkConclusion(principle.getConclusion())) {
				confirmedPrinciples.add(principle);
			}
		}
		for (final Principle principle : confirmedPrinciples) {
			processConfirmedConclusionBreathStraight(principle.getConclusion());
		}
	}

	private boolean checkConclusion(final String conclusion) {
        if (confirmedFacts.contains(conclusion)) {
            return true;
        }
        if (deniedFacts.contains(conclusion)) {
            return false;
        }
        final List<Principle> toCheck = knowledgeBase.getPrinciplesWithConclusion(conclusion);
        for (final Principle principle : toCheck) {
            if (checkPrinciple(principle)) {
                confirmedFacts.add(conclusion);
                return true;
            }
        }
        deniedFacts.add(conclusion);
        return false;
    }

    private boolean checkPrinciple(final Principle principle) {
        for (final String premise : principle.getPremises()) {
            if (deniedFacts.contains(premise)) {
                return false;
            }
        }
        final List<String> outPremises = knowledgeBase.getOutPremisesFromPrinciple(principle);
        if (!checkPremises(outPremises)) {
            return false;
        }
        final List<Principle> linkedPrinciples = new ArrayList<>();
        for (final String premise : principle.getPremises()) {
            linkedPrinciples.addAll(knowledgeBase.getPrinciplesWithConclusion(premise));
        }
        for (final Principle toCheck : linkedPrinciples) {
            if (!checkConclusion(toCheck.getConclusion())) {
                return false;
            }
        }
		log.add(principle);
        return true;
    }

	private boolean checkPremises(final Collection<String> premises) {
		for (final String premise : premises) {
			if (!confirmedFacts.contains(premise) && deniedFacts.contains(premise)) {
				return false;
			} else if (!confirmedFacts.contains(premise)) {
				if (knowledgeBase.isConclusion(premise)) {
					throw new IllegalArgumentException("premise is conclusion");
				} else {
					if (showDialogAndReturnResult(premise)) {
						confirmedFacts.add(premise);
					} else {
						deniedFacts.add(premise);
						return false;
					}
				}
			}
		}
		return true;
	}

	private boolean showDialogAndReturnResult(final String premise) {
		final QuestionDialog dialog = new QuestionDialog(this, premise, LogItem.createLog(log, knowledgeBase));
		return dialog.showDialog();
	}

	public static void main(String[] args) throws Exception {
		ShellMain shell = new ShellMain();
		shell.setVisible(true);
	}

	private class AlgorithmActionListener implements ActionListener {

		private final SearchAlgorithm algorithm;

		AlgorithmActionListener(final SearchAlgorithm algorithm) {
			this.algorithm = algorithm;
		}

		@Override
		public void actionPerformed(final ActionEvent e) {
			currentAlgorithm = algorithm;
		}
	}

	private class DirectionActionListener implements ActionListener {

		private final SearchDirection direction;

		DirectionActionListener(final SearchDirection direction) {
			this.direction = direction;
		}

		@Override
		public void actionPerformed(final ActionEvent e) {
			currentDirection = direction;
		}
	}

}
