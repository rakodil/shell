package com.example;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class PrincipleHandler extends DefaultHandler {

	private static final String PRINCIPLE_TAG = "principle";
	private static final String PREMISE_TAG = "premise";
	private static final String CONCLUSION_TAG = "conclusion";

	private String currentTag = "";
	private KnowledgeBase base;

	private String currentConclusion = "";
	private List<String> currentPremises = new ArrayList<>();

	public PrincipleHandler(final KnowledgeBase base) {
		base.clear();
		this.base = base;
	}


	@Override
	public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
		switch (qName) {
			case PRINCIPLE_TAG:
			case CONCLUSION_TAG:
			case PREMISE_TAG:
				currentTag = qName;
				break;
		}
	}

	@Override
	public void endElement(final String uri, final String localName, final String qName) throws SAXException {
		switch (qName) {
			case PRINCIPLE_TAG:
				base.addPrinciple(new Principle(currentPremises, currentConclusion));
				currentPremises = new ArrayList<>();
			case CONCLUSION_TAG:
			case PREMISE_TAG:
				currentTag = "";
				break;
		}
	}

	@Override
	public void characters(final char[] ch, final int start, final int length) throws SAXException {
		final String data = new String(ch, start, length);
		switch (currentTag) {
			case CONCLUSION_TAG:
				currentConclusion = data;
				break;
			case PREMISE_TAG:
				currentPremises.add(data);
				break;
		}
	}

}
