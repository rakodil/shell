package com.example;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ResultDialog extends JDialog {

	private final List<LogItem> log = new ArrayList<>();
	private final String result;

	public ResultDialog(final Frame owner, final List<LogItem> log, final String result) {
		super(owner);
		this.log.addAll(log);
		this.result = result;
		setSize(new Dimension(400, 200));
		setContentPane(createContent());
		setModal(true);
		setTitle("Результат");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	}

	private JPanel createContent() {
		final JPanel content = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridwidth = 2;
		constraints.ipady = 20;
		final JTextArea answer = new JTextArea(result, 1, 30);
		content.add(answer, constraints);
		constraints.gridy = 1;
		constraints.ipady = 0;
		constraints.gridwidth = 1;
		constraints.fill = GridBagConstraints.NONE;
		final JButton closeBtn = new JButton("Закрыть");
		closeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				setVisible(false);
			}
		});
		content.add(closeBtn, constraints);
		constraints.gridx = 1;
		final JButton explainBtn = new JButton("Вывод объяснения");
		explainBtn.setEnabled(!log.isEmpty());
		explainBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				final LogDialog dialog = new LogDialog(log);
				dialog.setVisible(true);
			}
		});
		content.add(explainBtn, constraints);
		return content;
	}
}
