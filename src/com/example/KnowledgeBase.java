package com.example;

import java.util.*;

public class KnowledgeBase {

	private final List<Principle> knowledge;

	public KnowledgeBase() {
        knowledge = new ArrayList<>();

	}

    private KnowledgeBase(final List<Principle> knowledge) {
        this.knowledge = knowledge;
    }

	public void clear() {
		knowledge.clear();
	}


    public KnowledgeBase getSubKnowledgeBaseFromConclusion(final String conclusion) {
        if (isPremise(conclusion)) {
            throw new IllegalArgumentException("conclusion is not final");
        }
        List<Principle> subKnowledge = getSubKnowledge(getByConclusion(conclusion));
        subKnowledge.add(getByConclusion(conclusion));
        return new KnowledgeBase(subKnowledge);
    }

    private List<Principle> getSubKnowledge(final Principle principle) {
        Collection<Principle> res = new HashSet<>();
        for (final String premise : principle.getPremises()) {
            res.addAll(getPrinciplesWithConclusion(premise));
        }
        Set<Principle> tmp = new HashSet<>();
        for (final Principle subPrinciple : res) {
            tmp.addAll(getSubKnowledge(subPrinciple));
        }
        res.addAll(tmp);
        return new ArrayList<>(res);
    }

	public boolean isPremise(final String message) {
		for (final Principle principle : knowledge) {
			if (principle.isPremise(message)) {
				return true;
			}
		}
		return false;
	}

	public boolean isUnprocessedOrStatement(final String conclusion) {
		int count = 0;
		for (final Principle principle : knowledge) {
			if (principle.isConclusion(conclusion) && principle.isVerified() == null) {
				count++;
			}
		}
		return count > 1;
	}

	public boolean isConclusion(final String message) {
		for (final Principle principle : knowledge) {
			if (principle.isConclusion(message)) {
				return true;
			}
		}
		return false;
	}

	public List<Principle> getAllInitialPrinciples() {
		final List<Principle> res = new ArrayList<>();
		for (final Principle principle : knowledge) {
			if (principle.getPremises().equals(getOutPremisesFromPrinciple(principle))) {
				res.add(principle);
			}
		}
		return res;
	}

	public boolean canDenyPrinciple(final Principle principle) {
		for (final Principle toCheck : getPrinciplesWithConclusion(principle.getConclusion())) {
			if (toCheck.isVerified() == null || toCheck.isVerified()) {
				return false;
			}
		}
		return true;
	}

	public List<Principle> getByPremise(final String premise) {
		final List<Principle> res = new ArrayList<>();
		for (final Principle principle : knowledge) {
			if (principle.isPremise(premise)) {
				res.add(principle);
			}
		}
		return res;
	}

	public Principle getByConclusion(final String conclusion) {
		for (final Principle principle : knowledge) {
			if (principle.isConclusion(conclusion)) {
				return principle;
			}
		}
		return null;
	}

	public List<Principle> getPrinciplesWithConclusion(final String conclusion) {
		List<Principle> res = new ArrayList<>();
		for (final Principle principle : knowledge) {
			if (principle.getValue().equals(conclusion)) {
				res.add(principle);
			}
		}
		return res;
	}

	public Principle getFirstUnprocessedPrinciple() {
        System.out.println("Knowledge: " + knowledge);
        for (final Principle principle : knowledge) {
			if (hasOutPremises(principle) && principle.isVerified() == null) {
                System.out.println("returning " + principle);
                return principle;
			}
		}
		return null;
	}

	private boolean hasOutPremises(Principle toCheck) {
		for (final String premise : toCheck.getPremises()) {
			if (!isConclusion(premise)) {
				return true;
			}
		}
		return false;
	}

	public boolean addPrinciple(final Principle principle) {
		return knowledge.add(principle);
	}

	public List<String> getOutPremisesFromPrinciple(final Principle principle) {
		final List<String> res = new ArrayList<>(principle.getPremises().size());
		for (final String premise : principle.getPremises()) {
			if (!isConclusion(premise)) {
				res.add(premise);
			}
		}
		return res;
	}

	public List<Principle> getPrinciplesWithPremise(final String premise) {
		List<Principle> res = new ArrayList<>();
		for (final Principle principle : knowledge) {
			if (principle.hasPremise(premise)) {
				res.add(principle);
			}
		}
		return res;
	}

    public boolean isEndPrinciple(final Principle principle) {
        return getPrinciplesWithPremise(principle.getConclusion()).isEmpty();
    }

    public Principle getInitialPrinciple() {
        for (final Principle principle : knowledge) {
            if (principle.isVerified() == null) {
                for (final String premise : principle.getPremises()) {
                    if (isConclusion(premise)) {
                        break;
                    }
                }
                return principle;
            }
        }
        return null;
    }

	public Principle getHypothesis() {
		for (final Principle principle : knowledge) {
			if (principle.isVerified() == null) {
				if (!isPremise(principle.getConclusion())) {
					return principle;
				}
			}
		}
		return null;
	}

	public List<Principle> getAllHypotheses() {
		final List<Principle> res = new ArrayList<>();
		for (final Principle principle : knowledge) {
			if (!isPremise(principle.getConclusion())) {
				res.add(principle);
			}
		}
		return res;
	}

	@Override
	public String toString() {
		return "KnowledgeBase{" +
				"knowledge=" + knowledge +
				'}';
	}
}
