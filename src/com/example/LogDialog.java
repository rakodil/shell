package com.example;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class LogDialog extends JDialog {
	private final List<LogItem> log = new ArrayList<>();

	public LogDialog(final List<LogItem> logItems) {
		super();
		log.addAll(logItems);
		setSize(new Dimension(500, 200));
		setContentPane(createContent());
		setModal(true);
		setTitle("Подсистема объяснений");
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	}

	private JPanel createContent() {
		final JPanel content = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		JTextArea text = new JTextArea(constructLog(), 8, 40);
		final JScrollPane scrollPane = new JScrollPane(text);
		text.setEditable(false);
		text.setLineWrap(true);
		content.add(scrollPane);
		final JButton close = new JButton("Закрыть");
		close.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				setVisible(false);
			}
		});
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		content.add(close, constraints);
		return content;
	}

	private String constructLog() {
		final StringBuffer buffer = new StringBuffer();
		for (final LogItem item : log) {
			buffer.append(constructOneMessage(item.getFromUser(), item.getFromKnowledge(), item.getConclusion())).append("\n");
		}
		return buffer.toString();
	}

	private String constructOneMessage(
			final List<String> fromUser,
			final List<String> fromKnowledge,
			final String conclusion
	) {
		final StringBuffer buffer = new StringBuffer("На основании факта ");
		for (final String premise : fromUser) {
			buffer.append(premise).append("(получено от пользователя)").append(",\nи ");
		}
		for (final String premise : fromKnowledge) {
			buffer.append(premise).append("(получено из ранее сработавшего правила)").append(",\nи ");
		}
		buffer.append(" правила ").append(constructPrinciple(fromUser, fromKnowledge, conclusion));
		buffer.append(" был получен факт ").append(conclusion);
		return buffer.toString();
	}

	private String constructPrinciple(
			final List<String> fromUser,
			final List<String> fromKnowledge,
			final String conclusion
	) {
		final StringBuffer buffer = new StringBuffer("Если ");
		for (final String premise : fromUser) {
			buffer.append(premise).append(",и ");
		}
		for (final String premise : fromKnowledge) {
			buffer.append(premise).append(",и ");
		}
		buffer.replace(buffer.lastIndexOf(",и "), buffer.length() - 1, ", то");
		buffer.append(conclusion).append(",\n");
		return buffer.toString();
	}
}
