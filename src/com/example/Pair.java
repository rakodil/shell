package com.example;

public class Pair<K,V> {
	private K key;
	private V value;

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}

	public Pair(K key, V value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String toString() {
		return key + " = " + value;
	}

	@Override
	public int hashCode() {
		return key.hashCode()*13 + (value == null ? 0 : value.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof Pair) {
			Pair pair=(Pair)obj;
			if (key != null ? !key.equals(pair.key) : pair.key != null) {
				return false;
			}
			if (value != null ? !value.equals(pair.value) : pair.value != null) {
				return false;
			}
			return true;
		}
		return false;
	}

	public void setKey(final K key) {
		this.key = key;
	}

	public void setValue(final V value) {
		this.value = value;
	}
}
