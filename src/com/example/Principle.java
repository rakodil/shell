package com.example;

import java.util.List;

public class Principle extends Pair<List<String>, String> {

	private Boolean verified = null;

	public Boolean isVerified() {
		return verified;
	}

	public void setVerified(final boolean verified) {
		this.verified = verified;
	}

	public Principle(final List<String> premises, final String conclusion) {
		super(premises, conclusion);
	}

	public boolean addToPremises(final String message) {
		return getKey().add(message);
	}

	public boolean isPremise(final String message) {
		return getKey().contains(message);
	}

	public boolean isConclusion(final String message) {
		return getValue().equals(message);
	}

	public List<String> getPremises() {
		return getKey();
	}

	public String getConclusion() {
		return getValue();
	}

	public boolean hasPremise(final String premise) {
		return getKey().contains(premise);
	}

	@Override
	public boolean equals(final Object o) {
		if (!(o instanceof Principle)) return false;
		Principle obj = (Principle) o;
		return obj.getValue().equals(getValue()) && getPremises().containsAll(obj.getPremises());
	}

    @Override
    public String toString() {
        return "Principle{" + super.toString() +
                " verified=" + verified +
                '}';
    }
}