package com.example;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class QuestionDialog extends JDialog {
	private boolean result = false;
	private final List<LogItem> log = new ArrayList<>();

	public QuestionDialog(final Frame owner, final String question, final List<LogItem> log) {
		super(owner);
		this.log.addAll(log);
		setModal(true);
		setSize(new Dimension(400, 200));
		setTitle("Вопрос");
		setContentPane(createOptionPane(question));
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	}

	private JPanel createOptionPane(final String question) {
		final JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		final JTextArea content = new JTextArea(question, 2, 20);
		content.setEditable(false);
		final JScrollPane scrollPane = new JScrollPane(content);
		constraints.gridwidth = 3;
		constraints.ipady = 40;
		constraints.gridx = 0;
		constraints.gridy = 0;
		panel.add(scrollPane, constraints);
		final JButton yesBtn = new JButton("Да");
		constraints.ipady = 0;
		constraints.gridwidth = 1;
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridy = 3;
		constraints.gridx = 0;
		constraints.anchor = GridBagConstraints.CENTER;
		yesBtn.addActionListener(new ValuesChangeListener(true));
		panel.add(yesBtn, constraints);
		final JButton noBtn = new JButton("Нет");
		constraints.gridx = GridBagConstraints.RELATIVE;
		noBtn.addActionListener(new ValuesChangeListener(false));
		panel.add(noBtn, constraints);
		final JButton explainBtn = new JButton("Вывод объяснения");
		panel.add(explainBtn, constraints);
		explainBtn.setEnabled(!log.isEmpty());
		explainBtn.addActionListener(new ExplanationListener());
		return panel;
	}

	private class ValuesChangeListener implements ActionListener {

		private final boolean value;

		private ValuesChangeListener(final boolean value) {
			this.value = value;
		}

		@Override
		public void actionPerformed(final ActionEvent e) {
			result = value;
			QuestionDialog.this.setVisible(false);
		}
	}

	private class ExplanationListener implements ActionListener {

		@Override
		public void actionPerformed(final ActionEvent e) {
			final LogDialog dialog = new LogDialog(log);
			dialog.setVisible(true);
		}
	}

	public boolean showDialog() {
		setVisible(true);
		return result;
	}

}
