package com.example;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LogItem {
	private final List<String> fromUser = new ArrayList<>();
	private final List<String> fromKnowledge = new ArrayList<>();
	private final String conclusion;

	public LogItem(final Principle principle, final KnowledgeBase base) {
		this.conclusion = principle.getConclusion();
		fromUser.addAll(base.getOutPremisesFromPrinciple(principle));
		fromKnowledge.addAll(principle.getPremises());
		fromKnowledge.removeAll(fromUser);
	}

	public static List<LogItem> createLog(final Collection<Principle> logItems, final KnowledgeBase base) {
		final List<LogItem> log = new ArrayList<>(logItems.size());
		for (final Principle logItem : logItems) {
			log.add(new LogItem(logItem, base));
		}
		return log;
	}

	public List<String> getFromUser() {
		return fromUser;
	}

	public List<String> getFromKnowledge() {
		return fromKnowledge;
	}

	public String getConclusion() {
		return conclusion;
	}
}
